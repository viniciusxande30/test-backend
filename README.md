# Cume Digital - Teste Fullstack Developer
Teste para recrutamento de desenvolvedor Laravel na Cume Digital.

## O TESTE
Desenvolver um micro sistema em Laravel, queremos ver as seguintes telas e regras de negocio.
- uma tela de login
- uma tela de cadastro de usuário
- uma tela interna com um CRUD de uma agenda de contatos (3 campos apenas, Nome | E-mail | Telefone)
- Na listagem de da agenda, listar apenas os contatos do usuário logado.
 
## TECNOLOGIA
- Laravel 5.3 ou superior.
- Banco de dados MySQL.

### LAYOUT
Fica a sua escolha! O importante é conhecermos mais sobre o seu trabalho. Será um diferencial se você usar fizer as  validações de input com o Laravel Validations e/ou criar controle de acesso por nível com Laravel Policies. 

## O QUE VAMOS ANALISAR
- Generalização e centralização de métodos e lógicas
- Comentários estruturados e bem explicados
- Segurança da aplicação
- Conhecimento em PHP
- Conhecimento em Laravel
- Conhecimento em MySQL
- Conhecimento em git
- Tratamento de erros

## INFORMAÇÕES ÚTEIS
Caso deseje utilizar migrations para a criação do banco de dados é necessário que defina a codificação (charset) utilizada nas tabelas, para evitar conflito de **charset** em versões diferentes do MySQL Server.

## ENTREGA DO TESTE
Para iniciar o teste, faça um fork deste repositório, crie uma branch com o seu nome e depois envie-nos o pull request. Não basta apenas clonar o repositório. Fazendo isso você não vai conseguir fazer push. Esse teste é utilizado para vaga de **fullstack developer**.
 
## DEADLINE
O prazo limite é o mesmo definido na entrevista.